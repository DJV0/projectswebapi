﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.BLL.Entities
{
    public enum TaskStateInfo
    {
        ToDo,
        InProgress,
        Done,
        Canceled,
    }
}
