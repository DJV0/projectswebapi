﻿using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
    }
}
