﻿using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using Projects.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(ITeamRepository teamRepository, IUserRepository userRepository, 
            ITaskRepository taskRepository, IProjectRepository projectRepository)
        {
            TeamRepository = teamRepository;
            UserRepository = userRepository;
            TaskRepository = taskRepository;
            ProjectRepository = projectRepository;
        }

        public ITeamRepository TeamRepository { get; }

        public IUserRepository UserRepository { get; }

        public ITaskRepository TaskRepository { get; }

        public IProjectRepository ProjectRepository { get; }

        public void Dispose()
        {
            
        }

        public void Save()
        {
            
        }
    }
}
